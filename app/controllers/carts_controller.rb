class CartsController < ApplicationController
 def show
 	 @cart=Cart.find(params[:id])
end
def destroy
	@cart = current_cart
	@cart.line_items.each do |item|
		@cart.emptycart(item.product.id)
	end
	@cart.destroy
	session[:cart_id] = nil
	redirect_to root_url
end
end
