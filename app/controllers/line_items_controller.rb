class LineItemsController < ApplicationController
  def create
  	@cart = current_cart
  	product = Product.find(params[:product_id])
  	@line_item = @cart.add_product(product.id)
  	if @line_item.save
  		redirect_to  root_url
  	else
  		redirect_to root_url	
  	end
  end
end
