class OrdersController < ApplicationController
    def new
  	@cart = current_cart
  	if @cart.line_items.empty?
  		redirect_to root_url
  		flash[:danger]="cart is empty"
  	else
              @user = current_user
  		  @order=Order.new
  	end
  end
  def create
    	       @cart = current_cart
  		@order=Order.new(order_params)
             @order.add_line_items_from_cart(@cart)
  		if @order.save
                    @order.line_items.each do |item|
                            ItemOrder.create(order_id: @order.id,name: item.product.proname,amount: item.quantity,price:item.product.proprice)
                   end
        	       redirect_to @order
  			 @cart.destroy
			 session[:cart_id] = nil
  		else
  			render "new"
  		end
  end
  def show
  	@cart = current_cart
  	@order=Order.find(params[:id])
  end
  def index
    @cart = current_cart
    @user = current_user
    @orders = Order.all
    @current_orders = []
    @orders.each do |order|
      if order.user_id == @user.id
        @current_orders<<order
      end
    end
  end
  def destroy
    @order = Order.find(params[:id])
    @order.destroy
    redirect_to orders_url
  end
  private
  def order_params
  	params.require(:order).permit(:user_id,:name,:address,:email)
  end
end
