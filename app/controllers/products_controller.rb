class ProductsController < ApplicationController
  def new
  	@product = Product.new
  end
   def create
  	@product = Product.new(product_parmas)
  	# @product.proprice.to_i
  	# @product.proamount.to_i
  	if @product.save
  		redirect_to @product
  	else
  		render 'new'
  	end
   end
   def destroy
   	@product = Product.find(params[:id]).destroy
   	redirect_to root_url
   end
   def edit
   	@product = Product.find(params[:id])
   end
   def update
   	@product = Product.find(params[:id])
   	if @product.update_attributes(product_parmas)
   		redirect_to @product
   	else
   		render 'new'
   	end
   end
   def show
   	@product = Product.find(params[:id])
   end
   def index
   	@products = Product.all
   end
   
   private
   def product_parmas
   	params.require(:product).permit(:proname,:proprice,:proamount,:picture)
   end
end
