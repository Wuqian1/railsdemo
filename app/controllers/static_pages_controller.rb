class StaticPagesController < ApplicationController
  def home
  	if logged_in?
  		@products = Product.all
  		@cart =current_cart
  	end
  end
end
