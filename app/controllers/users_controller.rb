class UsersController < ApplicationController
  def new
  	@user = User.new
  end
  def create
  	@user = User.new(user_params)
  	if @user.save
  		flash[:success] = "signup success"
              log_in @user
  		redirect_to @user
  	else
  		render "new"
  	end
  end
  def show
      @cart = current_cart
  	@user = User.find(params[:id])
  end
  def edit
      @cart = current_cart
      @user = User.find(params[:id])
  end
  def update
   @user = User.find(params[:id])
   if @user.update_attributes(user_params)
      flash[:success] = "update success"
      redirect_to @user
  else
    render "edit"
   end
  end
  private 
  def user_params
  	params.require(:user).permit(:name,:email,:password)
  end
end
