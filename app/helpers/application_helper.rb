module ApplicationHelper
	def baseTitle(diff='')
		base_title = "welcome depot"
		if diff.empty?
			base_title
		else
			"#{diff}|#{base_title}"
		end
	end
end
