class Cart < ActiveRecord::Base
	has_many :line_items,:dependent=>:destroy
	def add_product(product_id)
		current_item= line_items.find_by_product_id(product_id)
		if current_item
			current_item.quantity+=1
			current_item.product.proamount-=1
			current_item.product.update_attributes(:proamount=>current_item.product.proamount)
		else
			current_item = line_items.build(:product_id=>product_id)
			current_item.product.proamount-=1
			current_item.product.update_attributes(:proamount=>current_item.product.proamount)
		end
		current_item
	end
	def emptycart(product_id)
		current_item= line_items.find_by_product_id(product_id)
		current_item.product.proamount+=current_item.quantity
		current_item.product.update_attributes(:proamount=>current_item.product.proamount)
	end
	def total_price
		line_items.to_a.sum{|item| item.total_price}
	end

end
