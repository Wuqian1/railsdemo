class ItemOrder < ActiveRecord::Base
	belongs_to :order
	def total_price
		price * amount
	end
end
