class Order < ActiveRecord::Base
	default_scope -> { order(created_at: :desc) }
	has_many :line_items,:dependent=>:destroy
	has_many :item_orders,:dependent=>:destroy
	belongs_to :user
	def add_line_items_from_cart(cart)
		cart.line_items.each do |item|
			# item.cart_id=nil
			line_items<<item
		end
		
	end
	def total_price
		item_orders.to_a.sum{|item| item.total_price}
	end
end
