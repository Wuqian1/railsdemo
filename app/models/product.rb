class Product < ActiveRecord::Base
	default_scope -> { order(created_at: :desc) }
	has_many :line_items
	validates :proname,presence:true,length:{maximum:255}
	validates :proprice,presence:true,:numericality =>{:greater_than_or_equal_to =>0.01}
	validates :proamount,presence:true,:numericality =>{allow_nil: true}
	mount_uploader :picture, PictureUploader
end
