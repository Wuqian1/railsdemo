class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :proname
      t.integer :proprice
      t.integer :proamount

      t.timestamps
    end
  end
end
