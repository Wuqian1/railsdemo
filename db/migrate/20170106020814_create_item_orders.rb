class CreateItemOrders < ActiveRecord::Migration
  def change
    create_table :item_orders do |t|
      t.string :name
      t.integer :amount
      t.integer :price

      t.timestamps
    end
  end
end
