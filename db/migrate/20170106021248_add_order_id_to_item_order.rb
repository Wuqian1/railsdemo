class AddOrderIdToItemOrder < ActiveRecord::Migration
  def change
    add_column :item_orders, :order_id, :integer
  end
end
