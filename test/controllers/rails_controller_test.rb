require 'test_helper'

class RailsControllerTest < ActionController::TestCase
  test "should get destory" do
    get :destory
    assert_response :success
  end

  test "should get model" do
    get :model
    assert_response :success
  end

  test "should get products" do
    get :products
    assert_response :success
  end

end
